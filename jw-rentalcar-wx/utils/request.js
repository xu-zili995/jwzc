import constant from "./constant.js";

export const wxRequest = (options) => {
	return new Promise((resolve, reject) => {
		var Token = uni.getStorageSync("token");
		let contentType='application/json';
		if(options.method ==='POST'){
			contentType ='application/x-www-form-urlencoded'
		}
		uni.request({
			url: constant.BASE_URL + options.url, //仅为示例，并非真实的接口地址
			method: options.method || 'GET',
			data: options.data || {},
			header: {
				"content-type": contentType,
				"token":Token
			},
			success(res) {
				//如果状态码为401，则是 token过期失效，统一返回登陆界面重新登录
				if (res.data.code === 401) {
					uni.clearStorageSync();
					uni.setStorageSync('USERSTATUS',false)
					uni.showModal({
						title: "提示",
						content: "未登录或登录过期请先登录",
						showCancel: false, // 是否显示取消按钮，默认为 true
						success: function(res) {
							uni.navigateTo({
								url: '/pages/login/login'
							})
						},
					})
				} else {
					resolve(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: '请求接口失败',
				});
				reject(err)
			}
		})
	})
}