function showMsg(type,text,time=1500){
	this.$refs.toast.show({model:type,mask:false,label:text,wait:time})
}
module.exports ={
	showMsg
}