import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import tmVuetify from "./tm-vuetify";
Vue.use(tmVuetify)
import {wxRequest} from './utils/request'
import { showMsg } from 'utils/showMsg.js'
import io from './utils/weapp.socket.io.js'
Vue.prototype.socket = io('http://192.168.1.223:3050')
Vue.prototype.$showMsg = showMsg
Vue.prototype.$wxRequest = wxRequest
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif