const multer  = require('multer')
const uuid= require("uuid")
// 磁盘存储
const storage = multer.diskStorage({
    destination:function(req,file,cb){
      cb(null,"./public/upload")
    },
    filename:function(req,file,cb){
      // //获取文件的全称
       let filename = file.originalname
      // // 截取后缀
       let extension = filename.substr(filename.lastIndexOf(".")+1).toLowerCase();
      // // 随机数+后缀 组成文件名
       filename = uuid.v1()+"."+extension
      cb(null, filename)
    }
  })
  // // 设置limits 防止DOs攻击  最大上传文件大小为10MB
const MulterConfig = multer({ storage: storage ,limits:{fieldSize:10*1024*1024}})

  module.exports = MulterConfig



