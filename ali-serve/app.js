const express = require('express');
const cors = require('cors')
const bodyParser=require('express')
// const session = require('express-session')

const app = express()
const path = require('path');

// 导入路由模块
const TestRouter = require('./router/test')
const WxRouter = require('./router/wx')
const UploadRouter = require('./router/upload')
// 启动服务器
 const serve = app.listen(3050,()=>{
  console.log('服务器已启动，请勿关闭窗口!')
})
const io = require('socket.io')(serve)
io.on('connection',(socket)=>{
  console.log('socket连接成功！')
  // 接收前端消息
  socket.on('message',data=>{
    console.log(data)
    // 广播消息
    socket.emit("hello",data);

  })
})





// 托管静态资源文件夹
app.use(express.static('./public'))
app.use(express.static(path.join(__dirname,'public')));


//跨域
app.use(cors({
  origin:['*']
}));

app.use(bodyParser.urlencoded({ extended: false }));


// 使用路由
app.use('/al',TestRouter)
app.use('/wx',WxRouter)
app.use('/upload',UploadRouter)
// 使用错误处理中间件,拦截所有路由中抛出的错误
app.use((err,req,res,next)=>{
  //err 得到的中间件传递过来的错误
  console.log(err);
  //设置http响应的状态码
  res.status(500).send({code:500,msg:'服务器端错误'})
});

