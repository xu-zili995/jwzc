const express = require('express');
const mysql = require('mysql');
const pool = mysql.createPool({
  host:'localhost',//主机名
  port:'3306',//端口号
  user:'root',//用户名
  password:'root',//密码
  database:'alzc',//数据库名称
  connectTimeout:15//最大连接数量
})
module.exports = pool;